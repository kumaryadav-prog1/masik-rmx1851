#!/system/bin/sh

#Uncomment me to output sh -x of this script to /cache/phh/logs
#if [ -z "$debug" ];then
#	mkdir -p /cache/phh
#	debug=1 exec sh -x "$(readlink -f -- "$0")" > /cache/phh/logs 2>&1
#fi
mount -o bind /system/bin/wpa_supplicant /vendor/bin/hw/wpa_supplicant || true
mount -o bind /system/bin/hostapd /vendor/bin/hw/hostapd || true


mount -o bind /system/etc/audio_configs.xml /vendor/etc/audio_configs.xml || true
mount -o bind /system/etc/audio_output_policy.conf /vendor/etc/audio_output_policy.conf || true
mount -o bind /system/etc/audio/audio_policy_configuration.xml /vendor/etc/audio/audio_policy_configuration.xml || true
mount -o bind /system/etc/audio_effects.xml /vendor/etc/audio_effects.xml || true
